﻿using System;
using System.Windows.Forms;
using House.House;
using House.HouseBuilder;

namespace House
{
    public partial class HouseForm : Form
    {

        /// <summary>
        /// settings of house
        /// </summary>
        private readonly Settings _settings = new Settings();

        /// <summary>
        /// constructor
        /// </summary>
        public HouseForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// button of create house in autodesk
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Build_button_Click(object sender, EventArgs e)
        {
             try
             {
                CollectSetting();
                IHouseBuilder house = new HouseBuilderAutodesk(_settings);
                house.Build();
             }
             catch (FormatException)
             {
                MessageBox.Show(this, "Все поля должны быть заполнены", "Обнаружены ошибки", MessageBoxButtons.OK, MessageBoxIcon.Warning);
             }
             catch (ApplicationException exception)
             {
                MessageBox.Show(this, exception.Message, "Обнаружены ошибки", MessageBoxButtons.OK, MessageBoxIcon.Warning);
             }
             catch (Exception exception)
             {
                MessageBox.Show(this, exception.Message + " \n" + exception.StackTrace, "Обнаружены ошибки", MessageBoxButtons.OK, MessageBoxIcon.Warning);
             }
          
        }

        /// <summary>
        /// method collect settings from Form
        /// </summary>
        private void CollectSetting()
        {
           
            _settings.RoomsCount = int.Parse(textBoxCountRoomOnFloor.Text);
            _settings.FloorsCount = int.Parse(textBoxCountFloor.Text);
            _settings.PorchesCount = int.Parse(textBoxCountPorch.Text);

            _settings.LengthRoom = int.Parse(textBoxLengthRoom.Text);
            _settings.WidthRoom = int.Parse(textBoxWidthRoom.Text);
            _settings.HeightRoom = int.Parse(textBoxHeightRoom.Text);

            _settings.LengthPorchOnHall = int.Parse(textBoxLengthPorch.Text);
            _settings.WidthPorch = int.Parse(textBoxWidthPorch.Text);

            _settings.IsElevator = checkBoxLift.Checked;
            _settings.WidthWall = int.Parse(textBoxWidthWall.Text);

        
            _settings.Calculate();
          
        }

        /// <summary>
        /// this event checked, entered symbol was digit or backspace
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CheckNumberPress(object sender, KeyPressEventArgs e)
        {
            if (!Char.IsDigit(e.KeyChar) && e.KeyChar != 8)
            {
                e.Handled = true;
            }
        }
    }
}
