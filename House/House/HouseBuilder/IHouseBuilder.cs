﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace House
{
    /// <summary>
    ///  Interface for house builder
    /// </summary>
    interface IHouseBuilder
    {
        /// <summary>
        /// build house
        /// </summary>
        void Build();
    }
}
