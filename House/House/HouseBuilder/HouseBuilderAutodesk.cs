﻿using System.Collections.Generic;
using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.Geometry;
using House.House;
using Application = Autodesk.AutoCAD.ApplicationServices.Core.Application;

namespace House.HouseBuilder
{
    /// <summary>
    /// 
    /// </summary>
    class HouseBuilderAutodesk : IHouseBuilder
    {
        
        /// <summary>
        /// settings of house
        /// </summary>
        private readonly Settings _settings;
 
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="settings"></param>
        public HouseBuilderAutodesk(Settings settings)
        {
            _settings = settings;
        }

        /// <summary>
        /// Build house
        /// </summary>
        public void Build()
        {
            //настраиваем плагин для работы с autocad
            //открываем autocad document
            var _document = Application.DocumentManager.MdiActiveDocument;
           _document.LockDocument();
            var _database = _document.Database;

            using (Transaction transaction = _database.TransactionManager.StartTransaction())
            {
                // Открытие таблицы Блоков для чтения
                var blockTable = transaction.GetObject(_database.BlockTableId, OpenMode.ForRead) as BlockTable;

                // Открытие записи таблицы Блоков для записи
                var blockTableRecord = transaction.GetObject(blockTable[BlockTableRecord.ModelSpace],
                    OpenMode.ForWrite) as BlockTableRecord;

                //начинаем создание дома в центре координат
                var house = BuildHouse(new Point3d(0, 0, 0));
                //добавляем все элементы построенного дома в транзакцию
                addSolidToTransaction(blockTableRecord, transaction, house);
                transaction.Commit();
            }
        }

        /// <summary>
        /// build house, point - point of start building
        /// </summary>
        /// <param name="point">start building point</param>
        /// <returns>List of Solid3d</returns>
        private List<Solid3d> BuildHouse(Point3d point)
        {
            var house = new List<Solid3d>();
            //строим подъезды
            for (int i = _settings.PorchesCount - 1; i >= 0; i--)
            {
                 house.AddRange(BuildPorch(new Point3d(point.X + i * (_settings.LengthPorch), point.Y, point.Z))); 
            }
            //строим крышу
            var roof = BuildRoof(point);
            house.AddRange(roof);
            return house;
        }

        /// <summary>
        /// Build roof
        /// </summary>
        /// <param name="point">center of roof</param>
        /// <returns>List of Solid3d</returns>
        private List<Solid3d> BuildRoof(Point3d point)
        {
           var roofs = new List<Solid3d>();
           var pointRoof = new Point3d(point.X + (_settings.LengthPorch * _settings.PorchesCount) / 2 - _settings.LengthRoom / 2,
                                       point.Y + _settings.WidthPorch / 2 - _settings.WidthRoom / 2, 
                                       point.Z  +  _settings.HeightHouse);
           var roof = CreateSolid3D(_settings.LengthPorch * _settings.PorchesCount, _settings.WidthPorch, _settings.WidthWall, pointRoof);
           roofs.Add(roof);
           return roofs;
        }

        /// <summary>
        /// add List of solid to transaction
        /// </summary>
        /// <param name="blockTableRecord"></param>
        /// <param name="transaction"></param>
        /// <param name="solids"></param>
        private void addSolidToTransaction(BlockTableRecord blockTableRecord, Transaction transaction, List<Solid3d> solids)
        {
            foreach (var solid in solids)
            {
                blockTableRecord.AppendEntity(solid);
                transaction.AddNewlyCreatedDBObject(solid, true);
            }

        }


        /// <summary>
        /// Build porch
        /// </summary>
        /// <param name="point">center of porch</param>
        /// <returns>list of Solid3d</returns>
        private List<Solid3d> BuildPorch(Point3d point)
        {
            //строим холл подъезда
            var pointPorch = new Point3d(point.X, point.Y + _settings.WidthPorchOnFloor / 2, point.Z);
            var porch = BuildPorchHall(pointPorch);
            
            //строим ту часть подъезда, которая смежна с квартирами
            for (int i = _settings.FloorsCount - 1; i >= 0; i--)
            {
                  porch.AddRange(BuildFloor(new Point3d(point.X + _settings.LengthPorchOnHall, point.Y, point.Z + i * _settings.HeightRoom)));  
            }

            return porch;
        }

        /// <summary>
        /// Build porch hall
        /// </summary>
        /// <param name="point"></param>
        /// <returns></returns>
        private List<Solid3d> BuildPorchHall(Point3d point)
        {
            var porch = new List<Solid3d>();

            //строим площадки подъезда накаждом этаже
            for (int i = _settings.FloorsCount - 1; i >= 0; i--)
            {
                var pointFloor = new Point3d(point.X, point.Y, point.Z + i * _settings.HeightRoom);
                var floor = CreateSolid3D(_settings.LengthPorchOnHall, _settings.WidthPorch, _settings.WidthWall, pointFloor);

                var pointHole = new Point3d(point.X - _settings.LengthPorchOnHall / 2 + _settings.LengthPorchOnHall / 8 , point.Y, point.Z + i * _settings.HeightRoom);
                var holeForStairs = CreateSolid3D(_settings.LengthPorchOnHall / 4, _settings.WidthPorch, _settings.WidthWall, pointHole);
                if (i != 0)
                floor.BooleanOperation(BooleanOperationType.BoolSubtract, holeForStairs);
                porch.Add(floor);
            }

            //строим лестницы между этажами в подъезде. так как на последнем этаже лестница не нужна, ставим -2
            for (int i = 0; i <= _settings.FloorsCount - 2; i++)
            {
                var pointStairs = new Point3d(point.X - _settings.LengthPorchOnHall/2 + _settings.LengthPorchOnHall/8,
                                              point.Y + _settings.WidthPorch / 2, 
                                              point.Z + i*_settings.HeightRoom);
                var stairs = CreateStairs(pointStairs);
                porch.AddRange(stairs);
            }
          
            //построение общих частей подъезда
            var wallHeight = _settings.HeightRoom*_settings.FloorsCount;
            var changeY = _settings.WidthPorch/2;
            var changeX = _settings.LengthPorchOnHall/2;

            var pointWall = new Point3d(point.X, point.Y + changeY, wallHeight / 2);
            var rightWall = CreateSolid3D(_settings.LengthPorchOnHall, _settings.WidthWall, wallHeight, pointWall);
            
            //построение правой стены хола подъезда
            var pointDoor = new Point3d(pointWall.X, pointWall.Y, pointWall.Z - _settings.HeightHouse / 2 + 150);
            var door = CreateSolid3D(200, 150, 300, pointDoor);
            rightWall.BooleanOperation(BooleanOperationType.BoolSubtract, door);

            porch.Add(rightWall);

            //построение левой стены хола подъезда
            pointWall = new Point3d(point.X, point.Y - changeY, wallHeight / 2);
            var leftWall = CreateSolid3D(_settings.LengthPorchOnHall, _settings.WidthWall, wallHeight, pointWall);
            porch.Add(leftWall);

            //построение задней стены хола подъезда
            pointWall = new Point3d(point.X - changeX, point.Y, wallHeight / 2);
            var backWall = CreateSolid3D(_settings.WidthWall, _settings.WidthPorch, wallHeight, pointWall);
            porch.Add(backWall);

            //построение шахты лифта, если она есть
            if (_settings.IsElevator)
            {
                porch.AddRange(BuildElevator(point));
            }

            return porch;
        }


        /// <summary>
        /// Create stairs in porch
        /// </summary>
        /// <param name="pointStairs"></param>
        /// <returns></returns>
        private List<Solid3d> CreateStairs(Point3d pointStairs)
        {
            List<Solid3d> stairs = new List<Solid3d>();
            //количество ступенек
            var countStairs = 13;
            //ширина ступеньки
            var widthStair = 100;
            //подсчитываем высоту ступенек
            var heightStair = _settings.HeightRoom  / countStairs;

            //строим лестницу
            for (int i = countStairs; i >= 0; i--)
            {
                pointStairs = new Point3d(pointStairs.X, pointStairs.Y - widthStair, pointStairs.Z + heightStair);
                var stair = CreateSolid3D(_settings.LengthPorchOnHall / 4, (int) ( widthStair * 1.5) , heightStair, pointStairs);
                stairs.Add(stair);
            }

            return stairs;
        }

        /// <summary>
        /// Build elevator shaft
        /// </summary>
        /// <param name="point">cebter if shaft</param>
        /// <returns>List of Solid3d</returns>
        private List<Solid3d> BuildElevator(Point3d point)
        {
            var elevator = new List<Solid3d>();

            //длина стены шахты лифта
            var length = 200;
            var change = length / 2;


            var pointWall = new Point3d(point.X + change, point.Y, point.Z + _settings.HeightHouse / 2);
            var wall = CreateSolid3D(_settings.WidthWall, length, _settings.HeightHouse, pointWall);
            elevator.Add(wall);

            pointWall = new Point3d(point.X - change, point.Y, point.Z + _settings.HeightHouse / 2);
            wall = CreateSolid3D(_settings.WidthWall, length, _settings.HeightHouse, pointWall);
            elevator.Add(wall);

            pointWall = new Point3d(point.X, point.Y + change, point.Z + _settings.HeightHouse / 2);
            wall = CreateSolid3D(length, _settings.WidthWall, _settings.HeightHouse, pointWall);
            elevator.Add(wall);

            pointWall = new Point3d(point.X, point.Y - change, point.Z + _settings.HeightHouse / 2);
            wall = CreateSolid3D(length, _settings.WidthWall, _settings.HeightHouse, pointWall);

            //создание двери на каждом этаже шахты
            for (int i = _settings.FloorsCount - 1; i >= -1; i--)
            {
                var pointDoor = new Point3d(pointWall.X, pointWall.Y, point.Z + _settings.HeightRoom * i + _settings.HeightRoom + 125);
                var door = CreateSolid3D(100, 200, 250, pointDoor);
                wall.BooleanOperation(BooleanOperationType.BoolSubtract, door);
            }

            elevator.Add(wall);

            return elevator;
        }


        /// <summary>
        /// build floor, include part of porch in floor
        /// </summary>
        /// <param name="point">center of floor</param>
        /// <returns>list of Solid3D</returns>
        private List<Solid3d> BuildFloor(Point3d point)
        {
            List<Solid3d> rooms = new List<Solid3d>();
            
            //построение квартир на этаже
            for (int i = _settings.RoomsCount - 1; i >= 0; i--)
            {
                var pointRoom = new Point3d(point.X + i * _settings.LengthRoom, point.Y, point.Z);
                rooms.AddRange(BuildRoom(pointRoom));
            }

            //построение подъехда на этаже
            var lengthPorchOnFloor = _settings.LengthPorchOnFloor;
            var changeX = (lengthPorchOnFloor / 2) - (_settings.LengthRoom / 2);
            var changeY = (_settings.WidthRoom / 2) + (_settings.WidthPorchOnFloor / 2);
            var changeZ = _settings.HeightRoom / 2;

            //построение пола подъезда на этаже
            var pointFloor = new Point3d(point.X + changeX, 
                                         point.Y + changeY, 
                                         point.Z);

            var floor = CreateSolid3D(lengthPorchOnFloor, _settings.WidthPorchOnFloor, _settings.WidthWall, pointFloor);
            rooms.Add(floor);

            //построение задней стены
            var pointBackWall = new Point3d(point.X + lengthPorchOnFloor - _settings.LengthRoom / 2, 
                                            point.Y + changeY, 
                                            point.Z + changeZ);
            var backWall = CreateSolid3D(_settings.WidthWall, _settings.WidthPorchOnFloor, _settings.HeightRoom, pointBackWall);
            rooms.Add(backWall);

            //построение лицевой стены
            var pointFrontWall = new Point3d(point.X + changeX, 
                                             point.Y + _settings.WidthRoom / 2 + _settings.WidthPorchOnFloor, 
                                             point.Z + changeZ);
            var frontWall = CreateSolid3D(lengthPorchOnFloor, _settings.WidthWall, _settings.HeightRoom, pointFrontWall);
            rooms.Add(frontWall);


            return rooms;
        }

        /// <summary>
        /// Create room
        /// </summary>
        /// <param name="point"> center of room</param>
        /// <returns>List of Solid3d</returns>
        private List<Solid3d> BuildRoom(Point3d point)
        {
            var room = new List<Solid3d>();
            //построение пола
            var floorRoom = CreateSolid3D(_settings.LengthRoom, _settings.WidthRoom, _settings.WidthWall, point);
            room.Add(floorRoom);

            var wallHeight = point.Z + _settings.HeightRoom / 2;
            var changeX = _settings.LengthRoom / 2;
            var changeY = _settings.WidthRoom / 2;
            //построение правой стены
            var pointWall = new Point3d(point.X, point.Y + changeY, wallHeight);
            var rightWall = CreateSolid3D(_settings.LengthRoom, _settings.WidthWall, _settings.HeightRoom, pointWall);
            //вырезание из стены двери
            var pointDoor  = new Point3d(pointWall.X, pointWall.Y, pointWall.Z - _settings.HeightRoom/4);
            var door = CreateSolid3D(100, 200, _settings.HeightRoom / 2, pointDoor);
            rightWall.BooleanOperation(BooleanOperationType.BoolSubtract, door);
            room.Add(rightWall);

            //построение левой стены
            pointWall = new Point3d(point.X, point.Y - changeY, wallHeight);
            var leftWall = CreateSolid3D(_settings.LengthRoom, _settings.WidthWall, _settings.HeightRoom, pointWall);
            //вырезание из стены окна
            var pointWindow = new Point3d(pointWall.X, pointWall.Y, pointWall.Z);
            var window = CreateSolid3D(150, 150, 150, pointWindow);
            leftWall.BooleanOperation(BooleanOperationType.BoolSubtract, window);

            room.Add(leftWall);
            //построение передней стены
            pointWall = new Point3d(point.X + changeX, point.Y, wallHeight);
            var frontWall = CreateSolid3D(_settings.WidthWall, _settings.WidthRoom, _settings.HeightRoom, pointWall);
            room.Add(frontWall);
            //построение задней стены
            pointWall = new Point3d(point.X - changeX, point.Y, wallHeight);
            var backWall = CreateSolid3D(_settings.WidthWall, _settings.WidthRoom, _settings.HeightRoom, pointWall );
            room.Add(backWall);

            return room;
        }

        /// <summary>
        /// Create Solid3D Box
        /// </summary>
        /// <param name="x">length</param>
        /// <param name="y">width</param>
        /// <param name="z">Height</param>
        /// <param name="point">center of solid</param>
        /// <returns>Solid3D</returns>
        public Solid3d CreateSolid3D(int x, int y, int z, Point3d point)
        {
            var solid = new Solid3d();
            solid.CreateBox(x, y, z);
            solid.TransformBy(Matrix3d.Displacement(point - Point3d.Origin));
            return solid;
        }
        }
    }

