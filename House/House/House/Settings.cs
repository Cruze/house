﻿using System;


namespace House.House
{
    /// <summary>
    /// Класс настроек дома
    /// </summary>
    public class Settings
    {
        #region count
        /// <summary>
        /// Количество этажей
        /// </summary>
        public int FloorsCount { get; set; }

        /// <summary>
        /// Количество комнат на этаже
        /// </summary>
        public int RoomsCount { get; set; }

        /// <summary>
        /// количество подъездов
        /// </summary>
        public int PorchesCount { get; set; }
        #endregion  

        #region Room size
        /// <summary>
        /// Длина комнаты
        /// </summary>
        public int LengthRoom { get; set; }

        /// <summary>
        /// Ширина комнаты
        /// </summary>
        public int WidthRoom { get; set; }

        /// <summary>
        /// Высота комнаты
        /// </summary>
        public int HeightRoom { get; set; }
        #endregion

        #region Porch size
        /// <summary>
        /// Ширина подъезда на этаже
        /// </summary>
        public int WidthPorchOnFloor { get; private set; }

        /// <summary>
        /// Длина подъезда на этаже
        /// </summary>
        public int LengthPorchOnFloor { get; private set; }

        /// <summary>
        /// Длина подъезда в холле
        /// </summary>
        public int LengthPorchOnHall { get;  set; }

        /// <summary>
        /// Общая длина подъезда
        /// </summary>
        public int LengthPorch { get; private set; }

        /// <summary>
        /// Общая ширина подъезда
        /// </summary>
        public int WidthPorch { get;  set; }

        /// <summary>
        /// Наличие шахты лифта
        /// </summary>
        public bool IsElevator { get; set; }

        #endregion  
     
        #region House size and parametres
        /// <summary>
        /// Высота дома
        /// </summary>
        public int HeightHouse {  get;  private set; }

        /// <summary>
        /// Ширина стен
        /// </summary>
        public int WidthWall { get; set; }

        #endregion

        /// <summary>
        /// подсчитываем все параметры и проверяем их валидность
        /// </summary>
        public void Calculate()
        {
            HeightHouse = FloorsCount * HeightRoom;

            WidthPorchOnFloor = WidthPorch - WidthRoom;

            LengthPorchOnFloor = RoomsCount * LengthRoom;

            LengthPorch = LengthPorchOnHall + LengthPorchOnFloor;

            isValid();
        }

        /// <summary>
        /// Проверка валидности установленных параметров
        /// </summary>
        public void isValid()
        {

            string errors = string.Empty;

            if (FloorsCount < 1 || FloorsCount > 10)
            {
                errors += "Количествой этажей должно быть не менее 1 и не более 10\n";
            }
            if (PorchesCount < 1 || PorchesCount > 5)
            {
                errors += "Количество подъездов должно быть не менее 1 и не более 5\n";
            }
            if (RoomsCount < 1 || RoomsCount > 10)
            {
                errors += "Количество квартир на этаже в подъезде должно быть не менее 1 и не более 10\n";
            }
            if (IsElevator && FloorsCount == 1) {
                errors += "Зачем лифт в одноэтажном доме?! \n";
            }
            if (WidthWall < 5 || WidthWall > 15)
            {
                errors += "Ширина стен не должна быть меньше 5 и больше 15\n";
            }
            if (LengthRoom < 500 || LengthRoom > 2000){
                errors += "Длина комнаты не должна быть меньше 500 и больше 2000\n";
            }
            if (WidthRoom < 500 || WidthRoom > 2000)
            {
                errors += "Ширина комнаты не должна быть меньше 500 и больше 2000\n";
            }
            if (HeightRoom < 500 || HeightRoom > 1000)
            {
                errors += "Высота комнаты не должна быть меньше 500 и больше 2000\n";
            }
            if (LengthPorchOnHall < 1500 || LengthPorchOnHall > 2000)
            {
                errors += "Длина подъезда не должна быть меньше 1000 и больше 2000\n";
            }
            if (WidthPorchOnFloor < 500 || WidthPorchOnFloor > 1000)
            {
                errors += "Ширина подъезда не должна быть меньше ширины квартиры + 500 и больше ширины квартиры + 1000\n";
            }
            
            if (errors.Length != 0)
            {
                throw new ApplicationException(errors);
            }
        }

    }
}
