﻿using System;
using System.IO;
using Autodesk.AutoCAD.Runtime;
using House.House;
using House.HouseBuilder;

namespace House
{
    public class InitPlugin : IExtensionApplication
    {
        /// <summary>
        /// initialize plugion
        /// </summary>
        public void Initialize()
        {
            var form = new HouseForm();
            form.Show();
        }
        /// <summary>
        /// exit plugin
        /// </summary>
        public void Terminate()
        {
         
        }

        /// <summary>
        /// Нагрузочное тестирование
        /// </summary>
        [CommandMethod("TESTING")]
        public void Test()
        {
            System.Diagnostics.Stopwatch myStopwatch = new System.Diagnostics.Stopwatch();
            StreamWriter file = new StreamWriter("D:\\TestORSAPR.txt");
            for (int i = 0; i < 100; i++)
            {
                myStopwatch.Reset();
                myStopwatch.Start(); 

                // Строим со значениями по умолчанию 
                var settings = new Settings
                {
                    FloorsCount = 3,
                    PorchesCount = 2,
                    RoomsCount = 4,
                    HeightRoom = 500,
                    LengthRoom = 1000,
                    WidthRoom = 1000,
                    WidthPorch = 1500,
                    LengthPorchOnHall = 1500,
                    WidthWall = 10,
                    IsElevator = false
                };
                settings.Calculate();
                HouseBuilderAutodesk tower = new HouseBuilderAutodesk(settings);
                tower.Build();

                myStopwatch.Stop(); 

                TimeSpan ts = myStopwatch.Elapsed;
                string elapsedTime = String.Format("{0:f}", ts.Milliseconds);
                file.Write(elapsedTime + "\n");
            }
            file.Close();
        }
    }
    
}
