﻿namespace House
{
    partial class HouseForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonBuild = new System.Windows.Forms.Button();
            this.labelCountFloor = new System.Windows.Forms.Label();
            this.textBoxCountFloor = new System.Windows.Forms.TextBox();
            this.textBoxWidthWall = new System.Windows.Forms.TextBox();
            this.textBoxLengthPorch = new System.Windows.Forms.TextBox();
            this.textBoxCountPorch = new System.Windows.Forms.TextBox();
            this.labelWidthWall = new System.Windows.Forms.Label();
            this.labelLengthPorch = new System.Windows.Forms.Label();
            this.labelCountEntrances = new System.Windows.Forms.Label();
            this.labelCountApartmentOnFloor = new System.Windows.Forms.Label();
            this.textBoxCountRoomOnFloor = new System.Windows.Forms.TextBox();
            this.checkBoxLift = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.labelWidthRoom = new System.Windows.Forms.Label();
            this.labelLengthRoom = new System.Windows.Forms.Label();
            this.textBoxWidthRoom = new System.Windows.Forms.TextBox();
            this.textBoxLengthRoom = new System.Windows.Forms.TextBox();
            this.textBoxHeightRoom = new System.Windows.Forms.TextBox();
            this.labelHeightRoom = new System.Windows.Forms.Label();
            this.labelWidthPorch = new System.Windows.Forms.Label();
            this.textBoxWidthPorch = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // buttonBuild
            // 
            this.buttonBuild.Location = new System.Drawing.Point(207, 271);
            this.buttonBuild.Name = "buttonBuild";
            this.buttonBuild.Size = new System.Drawing.Size(75, 23);
            this.buttonBuild.TabIndex = 0;
            this.buttonBuild.Text = "Построить";
            this.buttonBuild.UseVisualStyleBackColor = true;
            this.buttonBuild.Click += new System.EventHandler(this.Build_button_Click);
            // 
            // labelCountFloor
            // 
            this.labelCountFloor.AutoSize = true;
            this.labelCountFloor.Location = new System.Drawing.Point(12, 10);
            this.labelCountFloor.Name = "labelCountFloor";
            this.labelCountFloor.Size = new System.Drawing.Size(106, 13);
            this.labelCountFloor.TabIndex = 1;
            this.labelCountFloor.Text = "Количество этажей";
            // 
            // textBoxCountFloor
            // 
            this.textBoxCountFloor.Location = new System.Drawing.Point(207, 7);
            this.textBoxCountFloor.Name = "textBoxCountFloor";
            this.textBoxCountFloor.Size = new System.Drawing.Size(100, 20);
            this.textBoxCountFloor.TabIndex = 2;
            this.textBoxCountFloor.Text = "3";
            this.textBoxCountFloor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckNumberPress);
            // 
            // textBoxWidthWall
            // 
            this.textBoxWidthWall.Location = new System.Drawing.Point(207, 217);
            this.textBoxWidthWall.Name = "textBoxWidthWall";
            this.textBoxWidthWall.Size = new System.Drawing.Size(100, 20);
            this.textBoxWidthWall.TabIndex = 5;
            this.textBoxWidthWall.Text = "10";
            this.textBoxWidthWall.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckNumberPress);
            // 
            // textBoxLengthPorch
            // 
            this.textBoxLengthPorch.Location = new System.Drawing.Point(207, 164);
            this.textBoxLengthPorch.Name = "textBoxLengthPorch";
            this.textBoxLengthPorch.Size = new System.Drawing.Size(100, 20);
            this.textBoxLengthPorch.TabIndex = 6;
            this.textBoxLengthPorch.Text = "1500";
            this.textBoxLengthPorch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckNumberPress);
            // 
            // textBoxCountPorch
            // 
            this.textBoxCountPorch.Location = new System.Drawing.Point(207, 33);
            this.textBoxCountPorch.Name = "textBoxCountPorch";
            this.textBoxCountPorch.Size = new System.Drawing.Size(100, 20);
            this.textBoxCountPorch.TabIndex = 7;
            this.textBoxCountPorch.Text = "2";
            this.textBoxCountPorch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckNumberPress);
            // 
            // labelWidthWall
            // 
            this.labelWidthWall.AutoSize = true;
            this.labelWidthWall.Location = new System.Drawing.Point(12, 220);
            this.labelWidthWall.Name = "labelWidthWall";
            this.labelWidthWall.Size = new System.Drawing.Size(72, 13);
            this.labelWidthWall.TabIndex = 8;
            this.labelWidthWall.Text = "Ширина стен";
            // 
            // labelLengthPorch
            // 
            this.labelLengthPorch.AutoSize = true;
            this.labelLengthPorch.Location = new System.Drawing.Point(12, 167);
            this.labelLengthPorch.Name = "labelLengthPorch";
            this.labelLengthPorch.Size = new System.Drawing.Size(92, 13);
            this.labelLengthPorch.TabIndex = 9;
            this.labelLengthPorch.Text = "Длина подъезда";
            // 
            // labelCountEntrances
            // 
            this.labelCountEntrances.AutoSize = true;
            this.labelCountEntrances.Location = new System.Drawing.Point(12, 36);
            this.labelCountEntrances.Name = "labelCountEntrances";
            this.labelCountEntrances.Size = new System.Drawing.Size(124, 13);
            this.labelCountEntrances.TabIndex = 10;
            this.labelCountEntrances.Text = "Количество подъездов";
            // 
            // labelCountApartmentOnFloor
            // 
            this.labelCountApartmentOnFloor.AutoSize = true;
            this.labelCountApartmentOnFloor.Location = new System.Drawing.Point(12, 62);
            this.labelCountApartmentOnFloor.Name = "labelCountApartmentOnFloor";
            this.labelCountApartmentOnFloor.Size = new System.Drawing.Size(179, 13);
            this.labelCountApartmentOnFloor.TabIndex = 11;
            this.labelCountApartmentOnFloor.Text = "Количество квартир на площадке";
            // 
            // textBoxCountRoomOnFloor
            // 
            this.textBoxCountRoomOnFloor.Location = new System.Drawing.Point(207, 59);
            this.textBoxCountRoomOnFloor.Name = "textBoxCountRoomOnFloor";
            this.textBoxCountRoomOnFloor.Size = new System.Drawing.Size(100, 20);
            this.textBoxCountRoomOnFloor.TabIndex = 12;
            this.textBoxCountRoomOnFloor.Text = "4";
            this.textBoxCountRoomOnFloor.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckNumberPress);
            // 
            // checkBoxLift
            // 
            this.checkBoxLift.AutoSize = true;
            this.checkBoxLift.Location = new System.Drawing.Point(207, 246);
            this.checkBoxLift.Name = "checkBoxLift";
            this.checkBoxLift.Size = new System.Drawing.Size(53, 17);
            this.checkBoxLift.TabIndex = 15;
            this.checkBoxLift.Text = "Лифт";
            this.checkBoxLift.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 246);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(119, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "Наличие шахты лифта";
            // 
            // labelWidthRoom
            // 
            this.labelWidthRoom.AutoSize = true;
            this.labelWidthRoom.Location = new System.Drawing.Point(11, 139);
            this.labelWidthRoom.Name = "labelWidthRoom";
            this.labelWidthRoom.Size = new System.Drawing.Size(98, 13);
            this.labelWidthRoom.TabIndex = 25;
            this.labelWidthRoom.Text = "Ширина квартиры";
            // 
            // labelLengthRoom
            // 
            this.labelLengthRoom.AutoSize = true;
            this.labelLengthRoom.Location = new System.Drawing.Point(11, 113);
            this.labelLengthRoom.Name = "labelLengthRoom";
            this.labelLengthRoom.Size = new System.Drawing.Size(92, 13);
            this.labelLengthRoom.TabIndex = 24;
            this.labelLengthRoom.Text = "Длина квартиры";
            // 
            // textBoxWidthRoom
            // 
            this.textBoxWidthRoom.Location = new System.Drawing.Point(207, 136);
            this.textBoxWidthRoom.Name = "textBoxWidthRoom";
            this.textBoxWidthRoom.Size = new System.Drawing.Size(100, 20);
            this.textBoxWidthRoom.TabIndex = 23;
            this.textBoxWidthRoom.Text = "1000";
            this.textBoxWidthRoom.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckNumberPress);
            // 
            // textBoxLengthRoom
            // 
            this.textBoxLengthRoom.Location = new System.Drawing.Point(207, 110);
            this.textBoxLengthRoom.Name = "textBoxLengthRoom";
            this.textBoxLengthRoom.Size = new System.Drawing.Size(100, 20);
            this.textBoxLengthRoom.TabIndex = 22;
            this.textBoxLengthRoom.Text = "1000";
            this.textBoxLengthRoom.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckNumberPress);
            // 
            // textBoxHeightRoom
            // 
            this.textBoxHeightRoom.Location = new System.Drawing.Point(207, 84);
            this.textBoxHeightRoom.Name = "textBoxHeightRoom";
            this.textBoxHeightRoom.Size = new System.Drawing.Size(100, 20);
            this.textBoxHeightRoom.TabIndex = 21;
            this.textBoxHeightRoom.Text = "500";
            this.textBoxHeightRoom.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckNumberPress);
            // 
            // labelHeightRoom
            // 
            this.labelHeightRoom.AutoSize = true;
            this.labelHeightRoom.Location = new System.Drawing.Point(12, 87);
            this.labelHeightRoom.Name = "labelHeightRoom";
            this.labelHeightRoom.Size = new System.Drawing.Size(97, 13);
            this.labelHeightRoom.TabIndex = 20;
            this.labelHeightRoom.Text = "Высота квартиры";
            // 
            // labelWidthPorch
            // 
            this.labelWidthPorch.AutoSize = true;
            this.labelWidthPorch.Location = new System.Drawing.Point(11, 194);
            this.labelWidthPorch.Name = "labelWidthPorch";
            this.labelWidthPorch.Size = new System.Drawing.Size(98, 13);
            this.labelWidthPorch.TabIndex = 27;
            this.labelWidthPorch.Text = "Ширина подъезда";
            // 
            // textBoxWidthPorch
            // 
            this.textBoxWidthPorch.Location = new System.Drawing.Point(207, 191);
            this.textBoxWidthPorch.Name = "textBoxWidthPorch";
            this.textBoxWidthPorch.Size = new System.Drawing.Size(100, 20);
            this.textBoxWidthPorch.TabIndex = 26;
            this.textBoxWidthPorch.Text = "1500";
            this.textBoxWidthPorch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CheckNumberPress);
            // 
            // HouseForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(311, 303);
            this.Controls.Add(this.labelWidthPorch);
            this.Controls.Add(this.textBoxWidthPorch);
            this.Controls.Add(this.labelWidthRoom);
            this.Controls.Add(this.labelLengthRoom);
            this.Controls.Add(this.textBoxWidthRoom);
            this.Controls.Add(this.textBoxLengthRoom);
            this.Controls.Add(this.textBoxHeightRoom);
            this.Controls.Add(this.labelHeightRoom);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.checkBoxLift);
            this.Controls.Add(this.textBoxCountRoomOnFloor);
            this.Controls.Add(this.labelCountApartmentOnFloor);
            this.Controls.Add(this.labelCountEntrances);
            this.Controls.Add(this.labelLengthPorch);
            this.Controls.Add(this.labelWidthWall);
            this.Controls.Add(this.textBoxCountPorch);
            this.Controls.Add(this.textBoxLengthPorch);
            this.Controls.Add(this.textBoxWidthWall);
            this.Controls.Add(this.textBoxCountFloor);
            this.Controls.Add(this.labelCountFloor);
            this.Controls.Add(this.buttonBuild);
            this.Name = "HouseForm";
            this.Text = "HouseForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonBuild;
        private System.Windows.Forms.Label labelCountFloor;
        private System.Windows.Forms.TextBox textBoxCountFloor;
        private System.Windows.Forms.TextBox textBoxWidthWall;
        private System.Windows.Forms.TextBox textBoxLengthPorch;
        private System.Windows.Forms.TextBox textBoxCountPorch;
        private System.Windows.Forms.Label labelWidthWall;
        private System.Windows.Forms.Label labelLengthPorch;
        private System.Windows.Forms.Label labelCountEntrances;
        private System.Windows.Forms.Label labelCountApartmentOnFloor;
        private System.Windows.Forms.TextBox textBoxCountRoomOnFloor;
        private System.Windows.Forms.CheckBox checkBoxLift;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labelWidthRoom;
        private System.Windows.Forms.Label labelLengthRoom;
        private System.Windows.Forms.TextBox textBoxWidthRoom;
        private System.Windows.Forms.TextBox textBoxLengthRoom;
        private System.Windows.Forms.TextBox textBoxHeightRoom;
        private System.Windows.Forms.Label labelHeightRoom;
        private System.Windows.Forms.Label labelWidthPorch;
        private System.Windows.Forms.TextBox textBoxWidthPorch;
    }
}