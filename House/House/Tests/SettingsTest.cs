﻿using System;
using House.House;
using NUnit.Framework; 

namespace House.Tests
{
    /// <summary>
    /// test settings of house
    /// </summary>
    class SettingsTest
    {
         [TestCase(1, 1, 1,   500, 500, 500,   1500, 1500, 5, false, TestName = "Тестируем минимальные допустимые значения")]
         [TestCase(10, 5, 10, 1000, 2000, 2000, 3000, 2000, 15, true, TestName = "Тестируем максимальные допустимые значения")]
         [TestCase(2, 3, 2,   600, 1000, 1000,  1700, 1700, 10, true, TestName = "Тестируем допустимые значения")]
         [TestCase(11, 6, 11, 1001, 2001, 2001, 2001, 2001, 16, false, TestName = "Тестируем значение выше максимального допустимого", ExpectedException = typeof(ApplicationException))]
         [TestCase(0, 0, 0, 499, 499, 499, 998, 1499, 4, false, TestName = "Тестируем значение ниже минимально допустимого", ExpectedException = typeof(ApplicationException))]
         [TestCase(1, 1, 1, 500, 500, 500, 100, 1500, 5, false, TestName = "Тестируем недостаточную ширину подъезда на этаже", ExpectedException = typeof(ApplicationException))]
         [TestCase(1, 5, 10, 1000, 2000, 2000, 3000, 2000, 15, true, TestName = "Тестируем отсутствие лифта в одноэтажном доме", ExpectedException = typeof(ApplicationException))]
        
        
        public void SettingTest(int countFloor, int countPorch, int countRoom, int heightRoom, int lengthRoom, int widthRoom, int widthPorch, int lengthPorchOnHall,
                                 int widthWall, bool elevator)
        {
             var settings = new Settings
             {
                 FloorsCount = countFloor,
                 PorchesCount = countPorch,
                 RoomsCount = countRoom,
                 HeightRoom = heightRoom,
                 LengthRoom = lengthRoom,
                 WidthRoom = widthRoom,
                 WidthPorch = widthPorch,
                 LengthPorchOnHall = lengthPorchOnHall,
                 WidthWall = widthWall,
                 IsElevator = elevator
             };

             settings.Calculate();
        }
    }
}
